import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Administrators from '../views/Administrators.vue'
import Staffs from '../views/Staffs.vue'
import SignIn from '../views/SignIn.vue'

global.baseURL = 'http://localhost:3000'; 

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/administrators',
    name: 'Administrators',
    component: Administrators
  },
  {
    path: '/staffs',
    name: 'Staffs',
    component: Staffs
  },
  {
    path: '/signin',
    name: 'SignIn',
    component: SignIn
  },
]

const router = new VueRouter({
  routes
})

export default router