var express = require('express');
const mongodb = require('mongodb');
const mongodbClient = require('mongodb').MongoClient;
var router = express.Router();

const url = 'mongodb+srv://benben1209:hi@cluster0.p5s3l.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

var options = {
    replSet: {
        sslValidate: false
    }
}

router.get('/findall', (req, res) => {
    mongodbClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic');
        dbo.collection('doctors').find().toArray((err, result) => {
            if (err) throw err;
            db.close();
            res.end(JSON.stringify(result))
        })
    })
})

router.post('/insertone', (req, res) => {
    let form = req.body;
    mongodbClient.connect(url, options,{ useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic');
        dbo.collection('doctors').insertOne(form, (err, result) => {
            if (err) throw err;
            db.close();
            res.end();
        })
    })
})

router.delete('/deleteone/:id', (req, res) => {
    let id = req.params.id
    mongodbClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic');
        let param = { _id: new mongodb.ObjectId(id) };
        dbo.collection('doctors').deleteOne(param, (err, result) => {
            if (err) throw err;
            db.close();
            res.end();
        })
    })
})

router.put('/updateOne/:id', (req, res) => {
    let id = { _id: new mongodb.ObjectId(req.params.id) }
    let form = { $set: req.body };
    mongodbClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic');
        dbo.collection('doctors').updateOne(id, form, (err, result) => {
            if (err) throw err;
            db.close();
            res.end();
        })
    })
})

router.get('/search/:searchParam', (req, res) => {
    let param = req.params.searchParam
    console.log(param)
    mongodbClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic');
        let query = { fullname: new RegExp(param, 'i') }
        dbo.collection('doctors').find(query).toArray((err, result) => {
            if (err) throw err;
            db.close();
            res.end(JSON.stringify(result));
        });
    });
});

module.exports = router;
